import express from "express"
import http from "http"
import path from "path"
import reload from "reload"

const currentPath = process.cwd()
const app = express()
const ROOTS = [
  "",
  "eliothing",
  "eliobones",
  "elioflesh",
  // "eliofaithful",
  // "elioolympians",
  // "eliorenaissance",
  "elioangels",
  "eliosin",
]
ROOTS.forEach(subjectOf => {
  let elioGroupUrl = subjectOf ? `/${subjectOf}` : ""
  let elioGroupDir = path.join(currentPath, subjectOf, "public")
  let elioGroupIndexHtml = path.join(elioGroupDir, "index.html")
  app.use(elioGroupUrl, express.static(elioGroupDir))
  app.get(elioGroupUrl, (req, res) => res.sendFile(elioGroupIndexHtml))
})

var server = http.createServer(app)

reload(app, { files: "html" })
  .then(r => {
    // reloadReturned is documented in the returns API in the README
    const PORT = process.env.PORT || 8080
    server.listen(PORT, () => {
      console.log(`Web server listening on port ${PORT}`)
    })
  })
  .catch(err => {
    console.error("Reload could not start FunTyper app:", err)
  })
