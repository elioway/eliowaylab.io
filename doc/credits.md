# elioWay Credits

## Core, ta very much!

- [Schema](https://schema.org)
- [Python](https://python.org)
- [NodeJs](https://nodejs.org)

## Artwork

- [wikiemedia/olive_trees_with_yellow_sky_and_sun](https://commons.wikimedia.org/wiki/File:Vincent_van_Gogh_-_Olive_Trees_with_Yellow_Sky_and_Sun.jpg)
- <http://2.images.southparkstudios.com/images/shows/south-park/episode-thumbnails/season-11/south-park-s11e09-more-crap_16x9.jpg?quality=0.8&grayscale=true>
- <http://2.images.southparkstudios.com/images/shows/south-park/episode-thumbnails/season-11/south-park-s11e09-more-crap_16x9.jpg?quality=0.8&grayscale=true>
- <http://book-graphics.blogspot.com/search?q=Paradise+Lost>

## Quora

What I learnt from Quora is the importance of footprint. The size of your site doesn't matter; what matters is uniqueness of content. So Quora uses canonical links to group questions it deems similar into 1 single "seo" bundle. Quoras difficulty was in group questions. At **the elioWay** we will ask users for a short, CamelCased name for their app - forcing users to create very "keyword" focused name to weed out "seo" weakness middleware. Maybe.

## Read This!

- <https://www.dublincore.org/specifications/dublin-core/dcmi-terms/> We want the richest schema..

## Read This If You're Offended

- <https://www.sparknotes.com/poetry/paradiselost/characters/>
- <https://publicdomainreview.org/collection/john-martin-s-illustrations-of-paradise-lost-1827>
