# wtf, JSON?

We're not going to teach you how to write **JSON** - only read it.

**JSON** is a _`data`_ "structure".

## wtf, data?

This is _`data`_:

```
"It talks."
```

This is _`data`_:

```
1
```

It's also a _`value`_. Means the same thing here.

This is _`data`_:

```
6000-01-01 BC
```

_`data`_ alone isn't very useful to computers, or humans. To give _`data`_ meaning and purpose in life we must put it into a "_`data`_ structure".

## wtf, _`data`_ structure?

"Structure" is simply what emerges get when you take some _`data`_ and organise it all together in a consistent way and descriptive way.

Every child has intuitively created _`data`_, in a structure, when they felttip-penned "5 Reasons I Like KPOP" at the top of a page - and wrote 5 bullet points below it (in different meaningful colours - "yellow because yellow makes me happy and dancing makes me happy").

## Data Structures in the Wild

This ain't all about computers, you know.

Below is a more formal _`data`_ structure called a "playscript". It has some of the _`data`_ from above, but now it sits with a structure that adds meaning to the _`data`_. Unlike the _`data`_ above, it is "self described" within the playscript. The playscript has "marked up" the _`data`_ so the actors know "TheCharacter: Says this" and "[TheCharacter does this]".

```
              =============
              ==  Act 1  ==
              == Scene 1 ==
              =============

[Eve enters stage right, followed by Adam.]

Eve: It talks.
Adams: Snakes which talk? Silly. How many?
Eve: [Pointing] 1\. There.
Adams: Oh. Hello, snake. When did you appear?
Eve: [Signalling the joke] Yesterday.
[Pause]
Eve: 6th of January, minus 6000 years BC.
[Pause while Adam blinks]
```

Since data isn't useful without structure, you'll find data structures everywhere. Road signs are a data structure. Maps are a data structures. Libraries are data structures.

## Computer Data Structures

Have you used Excel or another spreadsheet? Have you ever looked at, or opened up a **CSV** file?

You were working with a data structure!

Below is a data structure called "Comma Separated Values" [**CSV**]. Pay attention to the commas.

```
Name,Tempted By,Consequences,Where?,
satan,not tempted, just a pain in the ass,none
adam,eve,hell,eden
eve,satan,hell,eden
```

- The first row contains the "column names".
- All the other rows are _`records`_ with _`values`_ separated by commas.

These "column names" are more formally know as _`fields`_ or _`properties`_, i.e. the labels we give the _`data`_. "Jane" is not the same data as "John", but they are equally `firstName` _`properties`_ of a Person.

**CSV** is a common way to send data from one computer system to another. Computers understand it very well.

For humans it's hard to read and edit. We can see why just by looking it the `Name,Tempted by,Consequences` example above. Things don't line up! Imagine if there were 20 columns and 2000 rows? Impossible to read.

The smaller **CSV** file below is easier to read, even though "Pineapple" and "Strawberry" misalign, there isn't too much data:

```
Fruit,QTY,Each,CUR
Apple,4,2,USD
Pineapple,4,2,USD
Banano,4,2,USD
Strawberry,34,4,USD
Lemon,14,2,USD
```

## JSON, finally

**JSON** is another way of "structuring" the same data, but instead of having the column names in one place, and trying to line up the data below it, **JSON** is a _`data`_ structure that keeps the _property_ names with the _`data`_.

And for purposes of being very clear - the data in this **JSON** example has not changed from the **CSV** example above. Only the strutured changed. There are still 34 Strawberries @ $4 each.

```
[
  { "Fruit": "Apple", "QTY": 4, "Each": 2, "CUR": "$"  },
  { "Fruit": "Pineapple", "QTY": 4, "Each": 2, "CUR": "$" }
  { "Fruit": "Banano", "QTY": 4, "Each": 2, "CUR": "$" }
  { "Fruit": "Strawberry","QTY": 34, "Each": 44, "CUR": "£" }
  { "Fruit": "Lemon", "QTY": 14, "Each": 2, "CUR": "$" }
]
```

What? The price of Strawberries increased?! How did you spot that so quickly?

## How to read JSON

### Lists

In **JSON**, wrapping _`data`_ in square brackets - `[]` - indicates a **list** or `array`.

```
# This is an array
[1, 2, 3]

# This is an array
["a", "b", "c", "d"]

# This is an array
["1a", "b2", "c3", "4d", "e5", "6f"]

# This is just an array of three arrays:
[
  [x,o,o],
  [o,o,x],
  [x,x,o],
]

# This is just an array of three arrays of three arrays.
# Nothing to see here. Please more along:
[
  [
    [o,x,o],
    [x,x,o],
    [x,o,o],
  ],
  [
    [x,o,o],
    [o,o,x],
    [x,x,o],
  ],
  [
    [x,o,x],
    [o,x, ],
    [x, ,o],
  ],
]
```

## Objects

In **JSON** to assign _`data`_ as _`properties`_ we wrap in curly brackets - `{}` - and pair the _`data`_ with the _`property`_ name, like this:

```
{ "Fruit": "Apple", "QTY": 4, "Each": "$2" }
```

Still using commas... but in **CSV** the wrong comma, in the wrong place, could have you selling fruits called "$2" and costing "Apple". In **JSON** each comma separates a `property`:`data` pair (each is a love affair made in heaven).

We might call this "the Apple `object`".

In **JSON** this shows a _`record`_ with 3 main _`fields`_ or _`properties`_ (same thing here). Curly brackets show the _`object`_. The difference between an _`object`_ and a _`record`_, is that a _`record`_ is the whole _`object`_ (with possible sub _`objects`_).

Here's another simple object.

```
{
  firstName: "Adam",
  lastName: "One",
}
```

And **JSON** you can have _`arrays`_ of _`objects`_.

```
[
  {
    firstName: "Adam",
    lastName: "One",
  },
  {
    firstName: "Eve",
    lastName: "One",
  },
  {
    firstName: "Snake",
    lastName: "Istalkein",
  }
]
```

And in **JSON** you can have a _`record`_ (the highest level _`object`_) with a _`property`_ that is an _`object`_.

```
{
  firstName: "Adam",
  lastName: "One",
  spouse: {
    firstName: "Eve",
    lastName: "One",
    maidenName: "Two",
  },
}
```

Or where the _`property`_ is an _`array`_:

```
{
  firstName: "Adam",
  lastName: "One",
  chidren: ["Cain", "Abel"]
}
```

Or where the _`property`_ is an _`array`_ of `objects`:

```
{
  firstName: "Adam",
  lastName: "One",
  chidren: [
    { firstName: "Cain" },
    { firstName: "Abel" }
  ]
}
```

A _`record`_ can have other _`objects`_ inside them, but it doesn't make them _`records`_. If you have to get to the "sub `objects` through another _`object`_, then the "through _`object`_" is the _`record`_.

Below is a _`data`_ structure with 6 _`objects`_, but only 1 _`object`_ is the _`record`_.

```
# Nothing to see here:
{
  firstName: "Adam",
  lastName: "One",
  spouse: {
    firstName: "Eve",
    lastName: "One",
    maidenName: "Harlot",
    mother: {
      firstName: "Diana",
      lastName: "Harlot"
      mother: {
        firstName: "Carol",
        lastName: "Harlot"
        mother: {
          firstName: "Betty",
          lastName: "Harlot"
          mother: {
            firstName: "Anne",
            lastName: "Harlot"
          },
        },
      },
    },
  }
}
```

Below is a _`data`_ structure with 3 _`objects`_.

```
[
  {
    firstName: "Adam",
    lastName: "One",
  },
  {
    firstName: "Eve",
    lastName: "One",
  },
  {
    firstName: "Snake",
    lastName: "Istalkein",
  }
]
```

They are also 3 _`records`_ because they self contain a top level _`object`_.

That's it.

It got a bit much at the end, right? Sorry about that.

We're not trying to teach you to write **JSON** - only read it. Let's see what you learned.

How many Cats does this Human have?

```
[
  {
    id: snuggles,
    Type: Cat
  },
  {
    id: frieda,
    Type: Human,
    Cats: [
      { id: ginger,  Type: Cat },
      { id: tiger, Type: Cat },
      { id: rover, Type: Dog },
      { id: poopers, Type: Cat },
    ]
  },
]
```

If you answered this question, you understand **JSON** perfectly. It's possible you understand **JSON** perfectly even if you got the answer wrong: Because, my fallible, human friend, just because you know how to read a map, doesn't mean you will never get lost.

But it helps to know how to read a map anyway. And now you know how to read **JSON** you are well equipped to understand what it is we are doing, with **JSON**, **the elioWay**.

## What's Next?

- [elioWay dogma](/dogma.html) _(This is next)_

  - [elioThing dogma](/eliothing/dogma.html)
  - [elioBones dogma](/eliobones/dogma.html)
  - [elioFlesh dogma](/elioflesh/dogma.html)
  - [elioSin dogma](/eliosin/dogma.html)
