# Watch issues with gulp

If `gulp` crashes while running "watch", try this shell command in Linux.

```shell
echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
```
