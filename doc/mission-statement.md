# Mission Statement

> If you want to know how the wheel works, reinvent it. **Tim Bushell**

## Goal Focused

Provide a suite of development platforms and/or solutions, driven by <https://schema.org> data models, which will work, **the elioWay**.

These will be focused on making complicated things simple by insisting developers and users do `Things` the **elioWay**.

We aim to eliminate the need for cruddy code.

All applications will work the same way. They can be plugged together like lego. The data will be interchangeable.

Applications will be search engine optimized and compliant with <https://schema.org> micro-data right out of the box.

**elioWay** laws are small in number. We know what we have to do! We'll do it quickly.

## Learning focused

Intentionally break **elioWay** solutions into easily consumed, bite-sized libraries, which can be:

1. Comprehended by new developers;
2. Maintained by the inexperienced;
3. Used by the skilled to accelerate productivity.

**the elioWay** is a chance to play with new frameworks. It is not a framework. We learn frameworks so that we can make **elioWay** applications, even if (or especially if) they duplicate the features of other, better, more complex solutions.

We can refer back to **elioWay** projects; treat them like the most comprehensive, self contained, stackoverflowery answers for problems we come across in the future. We learn because we're **Goal Focused**.

## Nutshell

1. We'll learn by making frameworks work, **the elioWay**.
2. Solutions, working the **elioWay**, can be put into rapid production.

**Contributors needed.**
