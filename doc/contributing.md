# Contributing

Help us create the **elioWay**. Then we can all create other stuff, quickly, the **elioWay**.

## Development Roadmap

- **elioSin**. Prototype complete. Needs real world testing and hardening. Need some "starter" themes.
- **elioThing**. Prototype complete. Can build generic Schemas in JSON. Can adapt these for Mongo and Django. We have a **fakerjs** the **elioWay**.
- **elioBones**. Prototype complete. Tested. Express and Django versions need TLC. FeathersJS and Keystone versions low hanging fruit.
- **elioFlesh**. This is next. Need a NodeJS developer to create a nodejs only client frontend for **elioWay**. React, Vue and Django versions low hanging fruit.
- **elioWay**. White paper. Documentation blitz. Clarity.

## People Wish List

We need NodeJS developer(s) ASAP to complete the client app so we can demo a full working prototype (ironically the **elioWay** will be perfect for prototyping once it's complete).

We need website designers who know CSS to help create some designs for typical apps. Spacy or minimal, image focused, text focused, data entry focused, shop ready, etc.

We need content creators to start building the content for hundreds of elioApps. Recipes, how-tos, galleries, slideshows, videos, tourist guides, and more. The crazier the better.

We need developers of all abilities.

- Young developers. Help us and we will help you. **the elioWay** has hundreds of small coding jobs that you can do and learn doing. We'll show you the whole process - teach you code quality. In the meantime - your ignorance is useful. Sense check our documentation. Walk through the tutorials and make sure they work.

- Experienced developers. Help us and you will be helping yourself. You'll see straightway that what we're doing isn't brain surgery - but it will work. Your experience will be invaluable to catch yet unseen flaws or weakness in the **elioWay** design strategy and ensure maximum reusability.

Designers. Brand Managers. Project Managers. Writers. Technical writers. Administrators. Bloggers. We need you as well. Get in contact.

## Contributors Program

We want to reward and recognise people who help on **the elioWay**.

- elioFaithful: Doing their own thing, **the elioWay**.
- elioDisciple: Help out in any way.
- elioApostle: Adopt an app.
- elioProphet: Adopt a group.

[Contributors List](/contributors/contributors.json)

## Contributing

1. Fork this repo [contributors](https://elioway.gitlab.io/contributors)
2. `npm i -g @elioway/bones @elioway/thing`
3. Use the **bones** CLI to add yourself to the **list**
4. Submit a merge request.
