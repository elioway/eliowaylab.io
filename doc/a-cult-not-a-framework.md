# A cult, not a framework

**the elioWay** is a cult, not a framework. **elioWay** libraries _use_ frameworks **the elioWay**.

**elioWay** has simple laws, spelled out by the letters **e** **l** **i** **o**:

- **engage**: You can't **list** until you have **engaged** a `Thing`. You will...
- **list** `Thing`s of different types against the **engaged** `Thing`. You can...
- **iterate** to **engage** with any `Thing` you **list**. Feel free to...
- **optimize** by adding features, reports, validation and business logic.

We're going stop pretending one `Thing` is so unlike another and use <https://schema.org> Types as Models in every single application we make. Data - the **elioWay** - is modelled by a `Thing`. Each app will be loaded with an instance of `Thing`.

```
{
  name: "elioWay Engaged",
  url: "elioway.com",
  Person: { PersonThingFields },
  OtherSchema: { OtherSchemaThingFields },
  ItemList: [ ...ListOfThingsInVariousSchema ],
}
```

Any `Thing` listed can also be an App EntryPoint. Any **App** can load any **Data**. Data and Apps are interchangeable. In other words, on our platform, you can be an App developer, but you can also be a Data creator, or both. People will create Apps for Data, but Data can also be designed to target an App. Targeted or not, any App will read any Data and provision basic CRUD fuctions for it. Ideally, an App is Optimised to work with the semantic intent of the Data.

This is what we mean by "doing `Things` **the elioWay**", of various **elioWay** projects and apps - which use frameworks such as ExpressJs, DjangoRest, EmberJs, ReactJs, FeathersJs, Flask, Vue, and more - that .

**the elioWay** is a cult, not a framework.
