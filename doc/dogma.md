# elioWay Dogma

> so focused on the means: neglected the ways. **Tim Bushell**

**The elioWay** [TEW] is a [software architectural style](https://en.wikipedia.org/wiki/Software_architecture#Architectural_styles_and_patterns) or [design_pattern](https://en.wikipedia.org/wiki/Software_design_pattern).

In this "paper" we will represent that pattern using JSON objects. They don't have to be JSON objects. They could be classic SQL results. You can store your data in any system, and with any DB structure: Just do what works optimally for the scaling of your IT project.

- [A cult, not a framework](/a-cult-not-a-framework.html)
- [wtf is a Jason Oject?](/wtf-json.html)

JSON is how we will represent data on this page - and FYI it's also what we'll use initially to deliver some working prototypes and libraries utilising the proposed _design_pattern_.

Code examples will be shown in JavaScript - and FYI it's also what we'll use initially. You can use any language TEW.

## WTF

What it is not. TEW is not new. The pattern isn't even new. Many developers have probably used the same pattern before. What makes TEW stand out are the following attributes:

> All the TURDs... We're going to do them one. last. time. Squeeze out a bunch of world record, Randy Marsh sized CRAPs. Do them well.

### The pattern is sticky

A pattern isn't reusable and sharable if it isn't sticky. TEW aims to be sticky because the more people who adopt this _design_pattern_ in their development, the easier development becomes... as more and more sharable code - and data structured TEW - becomes available.

### nomenclature culture

We will be engaging and mnemonic. The chosen nomenclature used to described TEW _design_pattern_ is as much a part of the pattern as the technical aspects are. The names we've chosen for the various components of the pattern are meant to resonate with English speakers.

The _design_pattern_ is built upon the imagery and characters of **Paradise Lost**. Our documentation is informal. Our examples and tutorials feature quirky projects.

REST is dry. TEW is wacky.

### nomenclature outsourcing

We'll use the vocabulary of <https://schema.org> for all our data models. That changes things. The job of designing tables, choosing entity names, and deciding on field names will be gone. With TEW you have to find the best semantic match from [the full schema](https://schema.org/docs/full.html) and use that for the field or table name.

## TEW apps are micro apps

> We always end up with one big app that can do everything ... because everything in it is used by ONE person for a long time while it is being developed.

### Classical WebDev

For the first 6 months of the application life cycle, the developers are the only users. "We" (developers - yes we're fully authroised to speak for them all except Erik in Norway) think in terms of the whole database - and how we will enable a user to access, add, update, and... ideally... _use_ all that data. And for now there is just one user: The developer(s).

We always end up with one big app that can do everything, whether used by one person or more - because everything in it _is used_ by ONE person for a long time while it is being developed:

- Pages for adding, removing, editing, blah, blah, People.
- Pages for adding, removing, editing, blah, blah, Products.
- Pages for adding, removing, editing, blah, blah, Invoices.
- Pages for adding, removing, editing, blah, blah, Deliveries.
- Pages for adding, removing, editing, blah, blah, Suppliers.

and so on.

Once an app is deployed, everything changes. There isn't one user - there's many. And they aren't all using all of the app, only some of it. Different bits.

ZERO users are using _all_ the app like the developer did.

And with the "classical" way you end up with the same "editing Invoices Page" being used by Accounts and Customer services and... every damned person who ever uses it regardless of the actual task.

_Not always - sometimes different features of that same page are visible or not depending on access permissions (which we'll talk about). Nevertheless it's the same page._

### TEW WebDev

EW's _design_pattern_ pushes us another way.

Any app is "a way to help a user enlist the power of computing to complete a task". IN TEW, the database doesn't really exist. It's always the one "thing" - a single unit with a list - and to the app it's the whole universe.

- The Buying Department only needs an app to `add`, `list`, `edit` and `delete` `Product`s and `Part`s. They need a bundle of around 4 apps.
- The Marketing Department needs an app to `list` and `edit` `Product`s and `Part`s. They need a bundle of around 2 apps. Their "edit Products" page might need to focus on different fields than the Buying Department's.
- Customer Services needs only to `list` and `view` `Product`s - probably specific information about the `Product` - like it's `isoNumber` or the length of its `guarantee`. They need a bundle of around 2 apps. They will probably need 4 or 5 `Person` based apps.

The point is this: Teams of TEW developers aren't working on different features of the same app. They are always working on one feature of one app because each app only has one feature. It probably won't take them very long to build it. They won't need help.

And they won't need help because we've shortcut and time travelled to the future on every TEW project. When you start a TEW project, most of the work has already been done because it's likely that another TEW app with similar features has already been written and has all the endpoints just waiting for you to steal.

## TEW Dogma Stack

- **elioThing** provides schema and data models, and describes a*design_pattern*. Each record in the database is known as a "thing".
- **elioBones** provides the endpoints for manipulating one thing's data, according to the _design_pattern_.
- **elioFlesh** provides the UX, handles templating, and is responsible for pulling the four TEW layers of a TEW app together.

Optional:

- **elioSin** will work closely alongside **elioFlesh** providing tiny, classless, css stylesheets.

## Wrapping Up

1. TEW apps are small, easy to read, simple to debug, just made for unittests - and their features are agnostic.
2. The data from my TEW **Nodejs** app will work with your TEW **Rust** one.
3. Whether written in **Rust** or **Nodejs** or **python** - the app will always feel familiar to the user.
4. Porting an TEW app to a different runtime really means changing the **bones** file only.
5. Porting an TEW app in a language and framework you are familiar with, into one you are not - I.e **Nodejs** to **Rust** would be a good way to learn **Rust**.
6. Learning to code/config TEW apps, when you have never programmed, but are familiar with TEW apps, should be easier. TEW is a wonderful framework for learning how to code applications. Except it isn't a framework. It's a cult. TEW is a wonderful _design_pattern_ for learning how to code applications with whatever framework you want to learn.

## What's Next?

- elioWay dogma _(You are here)_

  - [elioThing dogma](/eliothing/dogma.html) _(This is next)_
  - [elioBones dogma](/eliobones/dogma.html)
  - [elioFlesh dogma](/elioflesh/dogma.html)
  - [elioSin dogma](/eliosin/dogma.html)
