# Hatched

**elioWay** was hatched on `2018-02-18`. That's just a fact. What follows is a true story. Some elements of the story have been exaggerated to little effect.

I'm Tim Bushell, leader in the **elioWay**. I've been working on this project, between paying gigs, for 5 years.

> "Everyone has this backstory!" "That's why you should tell it" **Tim Bushell**

## Backstory (optional)

I started creating "database driven web sites" at the turn of the century. And I was late... ish.

I bought an [Amstrad 6128](https://en.wikipedia.org/wiki/Amstrad_CPC#CPC6128) in 1983\. I was 16\. From then computers were central to my whole life - but it wasn't until ten years later that I started to code.

So if you were one of me, starting to code "database driven web sites" in the 1990s, first you had find and install a special "data adapter" in your programming lanaguage (my first was classic ASP with VBScript) and for your database (my first was Access). There were websites (before Stack) with lists of correctly formatted "connection stings" between Java, python, .NET and all the DBA.

Then you'd write a "data layer" to handle all the data calls and executions, between the server side script (tasked with generating the HTML) and the DB.

Often you'd take your version of that data layer with you between projects, but you'd still have to write all the custom SQL queries for the new project each time.

This is an old old problem we solve from scratch for each project. Easy but tiresome.

In ASP.NET, at ModuleSolutions in 2004, I conceive and helped build a "data layer code generator". That came with me to CourseMonster in 2007 - and I used it until 2013.

So I've been doing this stuff a lot, in one way or another, for a long time - but less time than some. And frankly... this occupation is in a bit of mess.

## State of non-elation

<figure>
  <img src="/artwork/south-park-s11e09c02-its-otherworldly-16x9.jpg" alt="south park more crap otherworldly">
</figure>

A lot has changed since the early days. Web frameworks now feature a lot of boilerplate stuff out of the box. I moved onto libraries like Django and Ember. Much easier. Write the names of your data tables; a list of fields; their datatypes; describe the relationships and... done. Then...

```
{% for peeps in mydata.people %}
  <!--
    write out an amazing contact list
    with all the data right where you
    need it for hardly any effort.
  -->
  ...
```

...in a script - that simple line - you'll have what it took an entire module of code to do in the olden days. Nice. I'm going to keep using tools like that, the **elioWay**.

But there is a flip side: Much is the same as before.

Some examples:

- The page where you write the line

```
{% for peeps in mydata.people %}
  <!--
    write out an amazing contact list
    with all the data right where you
    need it for hardly any effort.
  -->
  ...
```

... still has to be written. It's a crappy, tedious chore. "But it _does_ have to be written!", you declare. Correct. Every time. Why can't we change that?

- A `form` to write. And where you have to catch the click event of the form or the change event of an input... like every time:

```
onSubmit(theform=>crudify(theform))
```

And where you probably write:

```
<form class="form">
<table class="table">
<menu class="menu">
```

(if you're using Bootstrap). I kid you not. Not this time.

- The tedious selection process of checking out CSS frameworks until you settle for Bootstrap again simply because you have that file of common CSS utility class names special you that take with you between projects.

- Okay...Tailwind. Starting again with another framework... that's almost become the job.

- Wiring up the API.

- The data design.

Do we say `firstName` or `first_name` or `forename`? Is it `surname` or `lastName`. Surely it's `lastName` if you have `firstName`. Where do we put `middleNames`? What about the Pacific nation of Microfarfromus? Their people don't even have first names!

"Hmmm... That's hardly complicated, Tim", you say.

Right. But do you enjoy doing it?

That's just deciding on the field names of people - just wait until you're writing an app for a "molecule engineering startup" or some weird edge case exports company - which requires modeling complicated, mysterious objects.

> **crud** noun<br>
> A coating or an incrustation of filth or refuse. Something loathsome, despicable, or worthless. One who is contemptible or disgusting.

- And the forms!

**OMG**, the forms.

**OYG**, the forms!

All the gods, WTF!

Laying them out. Adding the "onSubmit" handler. Tackling the validation. It's just **CRUD**. The acronym isn't called **CRUD** for nothing. At the **elioWay** we call them all TURDs (takeup update read destroy). Neither are good acronyms. Sure: There is `create`, `read`, `update` and `delete` - but there is also "create with relationship to this", which is, for sure, just a special type of create (`create-create` or `create-update`) but special enough an operation you write so often you probably have a special function for it!

---

I want the **elioWay** to take us over the line and stop that all this **CRAP** (create read alter purge).

I love my job creating web applications - still - after 20 years: love the opportunity of putting the power of computing to trivial use for people daily with an unparrelled immediacy. For a olde gentlemen programmer like me, whose first Microsoft installation came on 10 or more floppy disks - and you had to sit there and feed the computer (like giving treats to dog) - the age of streaming is still a wonder.

But I don't want to write another form. I don't want to have to write "form.onSubmit" again. I don't want to have to say

```
peep = new Model(
  "Person",
  {
    "lastName": "string",
    "foreName": string,
    "dateOfBirth": "date"
  }
)
```

again. Can you imagine! Having to explain to a computer what a `Person` is?! These are our future rulers and they don't know what `Person` is yet?!

In 2022 we should not still need to write this:

```
<div class="container">
  <div="container-shell">
    <div="container-cushion">
      <div="centered green interesting">
        <p="but also nochalent">
          <span="shoot-me-now">
            { pageSubHeading }
          </span>
        </p>
      </div>
    </div>
  </div>
</div>
```

In 1981 (or whatever) I watched Blake ask a glass box to "plot a safe course to the Andromeda system". So why, in 2023, does the computer need my description of an `Address`, or for me to add a `deliveryDate` field to a Delivery object and explain how it needs to be a `Date` type?

In 2023, _I_, can't just say this:

```
person = new Model("Person")
```

...but in 1981 it can plot a course across the Milky Way avoiding asteroid fields and rogue gravity wells?!!!... fictionally of course... but... really??!!! And is there a pitch restriction to my outraged inner voice - because if there is... I haven't reached it yet!!!!!!

Or...

We could do it the **elioWay**.

Put better still: _I'm_ going to do it the **elioWay**. It will be worth it just for me.

<figure>
  <img src="/artwork/south-park-s11e09-more-crap_16x9.jpg" alt="south-park more crap squeezing one out">
</figure>

The **CRUD**.vAll the TURDs... I/We're going to do them one. last. time. Squeeze out a bunch of world record, Randy Marsh sized CRAPs. Do them well. Take that with us between projects, like those 200 lines of code in that data layer I wrote in VBScript waaaaaaay back in 1998 (or whatever). The data layer. The modelling. The HTML. The forms. The lot... 95% of it taken care of by the **elioWay**.

Let's aim to have this... at least, this:

```javascript
import { elioApp, ElioThing } from "theElioWay"
import thing from "./thing.json"
import bones from "./bones.js"
import flesh from "./flesh.json"

let PromotionsThing = ElioThing("Event", { list: ["Product", "Place"] })
let BusinessThing = ElioThing("Business", {
  list: ["Person", "Product", PromotionsThing],
})

let app = elioApp(BusinessThing, thing, bones, flesh)
app.run("localhost", 5000)
```

Combined (maybe) with an app another developer has written called `AdventCalendar`...

```javascript
import { elioApp, ElioThing } from "theElioWay"
import thing from "./thing.json"
import flesh from "./flesh.json"
import { AdventCalendar, bones } from "@eliofaithful/thisCouldBeYou"
AdventCalendar(thing, bones, flesh).run("localhost", 5000)
```

Which is really not intended to be pseudo code but an example of something which might be the whole app - made up by stacking lots of little apps with simple, data transforming endpoint you can predict... and if you help me write it we really could spin up working, targetted microapps with just a few lines of code and `imports`. Anyone could.

I want to spend less time writing CRUD and more time writing compelling and time saving features in simple micro applications for simple jobs without breaking the banks of small businesses, startups, hobbyists, charities and NGOs.

I'd love it if you help.
