# elioWay Quickstart

- [elioWay Prerequisites](/prerequisites.html)
- [Installing elioWay](/installing.html)

## Nutshell

- Want to model with <https://schema.org>, **the elioWay**? See [elioThing](/eliothing)
- Want to make server-side apis, **the elioWay**? See [elioBones](/eliobones)
- Want to make client-side apps, **the elioWay**? See [elioFlesh](/elioflesh)
- Want to make work on designing apps, **the elioWay**? See [elioSin](/eliosin)
- Want help with whatever you're doing? See [elioAngels](/elioangels)

## Making bacon

```shell
<pre>
# bacon descartes beccaria diderot hume kant leibniz locke montesquieu rousseau smith groot spinoza voltaire wollstonecraft geoffrin king necker astell depinay

set subjectOf elioway
set name elioenlightenment
mkdir -p $name
cd $name
yo thing:node --subjectOf $subjectOf --name $name
git init
git checkout -b main
git add .
git commit -m "hatched"
git remote add origin "git@gitlab.com:$subjectOf/$name.git"
git push --set-upstream origin main
cd ..
```
