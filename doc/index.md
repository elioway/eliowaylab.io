> It's 2023 and we still have to explain to a computer what a `Person` is. These are our future rulers and they don't know what `Person` is! **Tim Bushell**

# A Return to Paradise

<aside>
  <dl>
  <dd>Who would not, finding way, break loose from Hell,</dd>
  <dd>Though thither doomed! Thou wouldst thyself, no doubt</dd>
  <dd>And boldly venture to whatever place</dd>
  <dd>Farthest from pain, where thou mightst hope to change</dd>
  <dd>Torment with ease, and soonest recompense</dd>
  <dd>Dole with delight</dd>
</dl>
</aside>

Are you an application developer who, like me, is bored-witless writing the same boilerplate code, over and over again? Do you groan at the thought of building another form... rewiring validations... marshalling the onSubmit package just to capture the same old data? Does the mere idea of writing out yet another set of models for a pretty standard application darken your mood?

[Help complete and improve **the elioWay**](/contributors/quickstart.html) - a library for rapid application development; iteratively reusing a single methodic, mnemonic _design_pattern_ modeled on [SchemaOrg](https://schema.org) types.

Build applications **faster** with little to zero boilerplate code: Especially microapps, SPAs, webpages, components and other services with a similar footprint. Have **reusability** built into everything you do.

Use it to build your next client's project in a few hours, not days - or in minutes, not hours.

- [How To Contribute](/contributors/quickstart.html)

- [elioWay dogma](/dogma.html)

  - [elioThing dogma](/eliothing/dogma.html)
  - [elioBones dogma](/eliobones/dogma.html)
  - [elioFlesh dogma](/elioflesh/dogma.html)
  - [elioSin dogma](/eliosin/dogma.html)

## Neither new, nor complicated

> That's straight-up one to many! **Every developer ever**

We are the first to admit it. This pattern is neither new, nor complicated. It's basically **one to many**.

We call the "one" in this relationship the **engaged** thing. The "many" is what we call: the **list**.

All these records will be modelled on <http://schema.org> `Things`. Each App is for dealing with one **engaged** instance of a `Thing` and all the other "things" in its **list**.

Everything a developer needs to manage their data structured in the pattern described above will "come out of the box" in the **elioWay**, but you might have some specific features in mind for the application.

For example, in way you report on the information extracted out of the relationship between the **engaged** `Thing` and its list, or between any item in the list and the rest of the list. Maybe you need a custom widget to handle date entry?

This is where you start coding in the **elioWay** (all the CRUD is done for you) and we call this stage **optimise**.

That's the _design_pattern_ - a standard **way** of structuring and building applications spelled out by the letters of our way whose data is modelled on Schema: **e** **l** **i** **o**

## Features

<dl>
  <dt>Free Data Models!
</dt>
  <dd>
Data is modeled on Micro Data Schema from <a href="https://schema.org">https://schema.org</a>. <em>MDS</em> offers a ready collection of <em>schemaTypes</em>, like <code>Person</code>, <code>Place</code>, <code>Restaurent</code>, <code>EmailMessage</code>, <code>Event</code></dd>
  <dt>Search Engine Optimized
</dt>
  <dd><strong>elioWay</strong> applications publish their data in the correct format with <em>MDS</em> by default.</dd>
  <dt>Applications are "legoey"
</dt>
  <dd>
An <strong>elioWay</strong> application can be for any one or combination of those <em>schemaTypes</em> OR <strong>elioWay</strong> applications - so each app can be combined with other apps.</dd>
  <dt>Every <code>Thing</code> is an App
</dt>
  <dd>
When you <em>engage</em> some <code>Thing</code> in the <em>list</em>, you are not merely accessing data inside the using currently <em>engaged</em> app... like opening a <code>.doc</code> file in <em>MSWord</em>. You are <em>Engaging</em> another App. That app may or may not have the same features as its host.</dd>
  <dt>Every App can read any <code>Thing</code></dt>
  <dd><em>Every App will know how to read <em>*the elioWay</em></em> data structure.
In this way, the same data object can be <em>engaged</em> in different ways by combining it with different apps.</dd>
</dl>

## TLR

- [A cult, not a framework](/a-cult-not-a-framework.html)
- [But not a literal cult](/but-not-a-literal-cult.html)
- [Mission Statement](/mission-statement.html)
- [An idea is hatched](/hatched.html)

## The elioWay

To control the work flow and cope with the modular approach, **the elioWay** repository is split into GitLab groups.

<section>
  <figure>
  <a href="/eliothing/artwork/splash.jpg" target="_splash"><img src="/eliothing/artwork/splash.jpg" target="_splash"></a>
  <h3>Cult of Thing</h3>
  <p>Data models and database structure, the <strong>elioWay</strong>.</p>
  <p><a href="eliothing"><button><img src="/eliothing/favicoff.png">Thing</button></a></p>
</figure>
  <figure>
  <a href="/eliobones/artwork/splash.jpg" target="_splash"><img src="/eliobones/artwork/splash.jpg" target="_splash"></a>
  <h3>Bone Cult</h3>
  <p>APIs and Middleware, the <strong>elioWay</strong></p>
  <p><a href="eliobones"><button><img src="/eliobones/favicoff.png">Bones</button></a></p>
</figure>
  <figure>
  <a href="/elioflesh/artwork/splash.jpg" target="_splash"><img src="/elioflesh/artwork/splash.jpg" target="_splash"></a>
  <h3>Flesh Cult</h3>
  <p>Client apps, the <strong>elioWay</strong>.</p>
  <p><a href="elioflesh"><button><img src="/elioflesh/favicoff.png">Flesh</button></a></p>
</figure>
</section>

<section>
  <figure>
  <a href="/elioangels/artwork/splash.jpg" target="_splash"><img src="/elioangels/artwork/splash.jpg" target="_splash"></a>
  <h3>Angel Cult</h3>
  <p>Command line helpers, utilities and plugins <strong>elioWay</strong>.</p>
  <p><a href="elioangels"><button><img src="/elioangels/favicoff.png">Angels</button></a></p>
</figure>
  <figure>
  <a href="/eliosin/artwork/splash.jpg" target="_splash"><img src="/eliosin/artwork/splash.jpg" target="_splash"></a>
  <h3>Cult of Sin</h3>
  <p>A classless, wireframing CSS framework, the <strong>elioWay</strong>.</p>
  <p><a href="eliosin"><button><img src="/eliosin/favicoff.png">Sin</button></a></p>
</figure>
</section>

## Who is this for?

Developers can help us build a library of common code which they themselves can use to shortcut many types of applications and websites you build every day.

Startups can use it to showcase prototypes quickly.

Content Creators can format their media and text **the elioWay** - then use any app from the elioVerse to publish it... for instance `tiktokTheElioWay`, or `faceBookTheElioWay`, or `myCustomAppTheElioWay`.

Small business can use it to spin up micro apps for dealing with a short term problem... like a parents group organising a one off bake sale for some school trip - or as simple as spinning up a specific order with an invoice for a particular customer.

AI developers can train their minds with it - it's very atomised and easy to grade. If I knew AI, I would write tests for my endpoints - tests for training - AI can write code.

## What do elioWay apps look like?

### Data

The data - the `Thing` - might look like this:

```json
{
  "name": "AppName",
  ...ThingProperties,
  "Place": {...PlaceProperties},
  "Hospital": { ...HospitalProperties },
  "Itemlist": {
    "itemlistElement": [
      { "name": "Item1" },
      { "name": "Item2" },
    ]
  }
}
```

### Code

The application logic - the `Bone` - might look like:

```javascript
{
  potentialAction: (thing) => thing.potentialAction !== "OK",
  Itemlist: {
    All: (itemlist) => itemlist.filter(),
    Completed: (itemlist) => itemlist.filter(completedFilter),
  }
}
```

### Template

The template might look like:

```json
{
  "name": { "believer": "h1" },
  "description": { "believer": "p" },
  "status": { "believer": "dl" },
  "Itemlist": {
    "name": { "believer": "h3" }
  }
}
```

### Appification

And it would all come together like this:

```javascript
import { elioApp, ElioThing } from "theElioWay"
import thing from "./thing.json"
import bones from "./bones.js"
import flesh from "./flesh.json"

let PromotionsThing = new ElioThing("Event", { list: ["Product", "Place"] })
let BusinessThing = new ElioThing("Business", {
  list: ["Person", "Product", PromotionsThing],
})

let app = elioApp(BusinessThing, thing, bones, flesh)
app.run("localhost", 5000)
```
