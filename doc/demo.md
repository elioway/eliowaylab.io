# elioWay Demo

Install `@elioway/thing @elioway/bones generator-sin generator-thing`

```
thing
thing Action
thing ActionAccessSpecification
thing Hospital

thing Action --schema --depth=4

thing DeliveryEvent --write .

for T in (thing CreativeWork --list)
   thing $T --list
end
```
