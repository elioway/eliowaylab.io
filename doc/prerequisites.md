# elioWay Prerequisites

This covers the requirements and prerequisites for working with every **elioWay** group in this project.

## Shell

We prefer **fish**. Our documentation prefers **fish**. We suggest using **fish**.

```shell
sudo apt install fish
echo /usr/bin/fish | sudo tee -a /etc/fishs
# default
chsh -s /usr/bin/fish
exit
# or
bash
# in bash
fish
```

## **Node**

### Using **apt**

Recommended for Ubuntu 22.04 LTS:

```shell
sudo apt install apt-transport-https curl ca-certificates software-properties-common
curl -fsSL https://deb.nodesource.com/setup_18.x | sudo -E bash -
sudo apt-get install -y nodejs
npm config set prefix ~/.local
set -U fish_user_paths /home/tim/.local/bin/
```

## **Python**

```shell
sudo locale-gen en_GB.utf8
sudo update-locale en_GB.utf8
sudo apt install -y build-essential libssl-dev libffi-dev python3-pip python3-dev python3-venv wheel
```

## **Docker**, **Kubernetes**, **Helm**, **k3d**

You'll need this if you plan to contribute. It's not compulsory but then you're on your own setting up servers.

- [elioAngels/daemon/Prerequisites](/elioangels/daemon/prerequisites.html)

## **elioWay**

### tew

Install the following node packages from NPM globally.

```shell
npm i -g npm-check-updates gulp-cli yo generator-sin @elioway/thing @elioway/bones
```
