# But not a literal cult

It's a cult in the sense that it works best if you have a fierce fanaticism to engaging/listing/iterating/optimising Things **the elioWay** - and evangelising the idea so that we can all work together on simple projects more easily - but not in the religious sense where we believe it to be a factual truth. The elioWay is simply an attempt to "legoise" webapp development.

This is why we have incorporated the theological imagery and narrative of Milton's _Paradise_Lost_ in our project names and imagery. Memetic devices work, and in **the elioWay**, nothing works better for memomic "devices" than creation mythology - since we're all about creating "things".

The concept of **Bones**, **Flesh** and **Thing** came about because "Bone", "Flesh", "Thing" are all words whose direct translations in any language resonate with speakers. In English they are familiar in phrases like "of the flesh"; "chilled to the bone"; "first thing" - and they connect well with the general architecture of a website: DB, Client, Data.

As a metaphor for an API, "Bone" works because bones don't change. As a metaphor for the presentation layer, the frontend of the application or website, "Flesh" works because it's the Thing you see and touch.

The terms loosely tie three different aspects of web development and architecture in simple categories. But they are not so rigorous that Bones can't accomodate low level middleware or that Flesh can't accomodate high level middleware, templating engines, etc.

But we might change all that. We'll change it sooner rather that later. And we'll change for good semantic reasons, not for dogma. And if we don't change something, it's not because of dogma, it's because we are primarily about promoting a certain standard which won't appreciate backward compatibility issues... because that's our dogma.
