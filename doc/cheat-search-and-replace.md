# Cheat Search and Replace

Here's my deal. I want to S&R every occurence of "

<something>" and replace it, but only files in the <code>doc</code> folder in the elioway folder structure.</something>

A quick way would be to move all the doc folders, in their current structure, and put them in a different folder.

Later, you can just move them back - saying YES to merging folders.

```
set extractFolder "doc"
set extractToFolder "elioDocs"
rm -rf $extractToFolder
for D in (find . -type d -name node_modules  -prune -o -name $extractFolder -print)
    set ND (string sub -s 3 $D)
    echo $ND
    mkdir -p "$extractToFolder/$ND"
    # rsync -amP --remove-source-files "$D/" "$extractToFolder/$ND/"
end
```
