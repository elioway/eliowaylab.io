![](https://elioway.gitlab.io/elio-Way-logo.png)

> A cult, not a framework, **the elioWay**

# elioWay ![experimental](https://elioway.gitlab.io/eliosin/icon/devops/experimental/favicon.ico "experimental")

## WTF

**the elioWay** is a web and application development [_design_pattern_](./dogma/html) accompanied by a growing library of supporting code to help build applications, especially component driven "microapps", rapidly and consistently in a way which means everything you do can be reused.

The _design_pattern_ is neither new, nor complicated. It's straight-up **one to many**. **the elioWay** is proposing a conceptually simple standard for web and application development which, if we all follow, will ensure we can share our data and apps.

The "one" we call the **engaged** App. The "many" we call the **list**. You can **engage** one of the records in the **list**. It becomes the **engaged** App. It could have a **list** ... so we can **iterate** this pattern.

All these records will be modelled on schema.org `Things`. Each App is for dealing with one **engaged** instance of a `Thing` and its **list**.

Everything mentioned above will "come out of the box" in the **elioWay**, but you might have some specific features in mind for the application - for example in way you present the information extracted out of the relationship between the **engaged** `Thing` and its List, or between any item in the List and the rest of the List. This is where you start coding in the **elioWay** (all the CRUD is done for you) and we call this stage **optimise**.

In this way, all **elioWay** apps work with the same data structure. Data created by one App (gathering contact details of parent volunteers and students for a school trip) can be loaded into another app (ticking students on and off the bus) or into another app (buying a group ticket at the musuem) and so. Interchangable data, app and even features.

This is what were doing here... the **elioWay**.

- **engage**: You can't **list** until you have **engaged** a `Thing`. You will...
- **list** `Thing`s of different types against the **engaged** `Thing`. You can...
- **iterate** to **engage** with any `Thing` you **list**. Feel free to...
- **optimize** by adding features, reports, validation and business logic.

- [elioway Documentation](https://elioway.gitlab.io)

### **the elioWay**

- [elioAngels](https://elioway.gitlab.io/elioangels)
- [elioSin](https://elioway.gitlab.io/eliosin)
- [elioThing](https://elioway.gitlab.io/eliothing)
- [elioBones](https://elioway.gitlab.io/eliobones)
- [elioFlesh](https://elioway.gitlab.io/elioflesh)

![](https://elioway.gitlab.io/apple-touch-icon.png)

## License

[MIT](LICENSE) [Tim Bushell](mailto:theElioWay@gmail.com)
