jQuery.fn.extend({
  adonHideOnScrollDown: function () {
    ;(tags = $(this)),
      (position = $(window).scrollTop()),
      tags.each(function (t) {
        $(this).addClass("adonHideOnScrollDown"),
          $(this).mouseover(function () {
            $(this).addClass("adonHover")
          }),
          $(this).mouseout(function () {
            $(this).removeClass("adonHover")
          })
      }),
      $(window).scroll(function () {
        tags.each(function (t) {
          $(window).scrollTop() > position
            ? $(this)._adonHideIfNotHovering()
            : $(this).removeClass("adonHide")
        }),
          (position = $(window).scrollTop())
      }),
      setInterval(function () {
        ;(curr_position = $(window).scrollTop()) === position &&
          tags.each(function (t) {
            $(this)._adonHideIfNotHovering()
          })
      }, 2e3)
  },
  _adonHideIfNotHovering: function () {
    $(this).hasClass("adonHover") || $(this).addClass("adonHide")
  },
}),
  $(document).ready(function () {
    $("[href]").each(function () {
      $(this).href == window.location.href && $(this).addClass("active")
    })
  }),
  (String.prototype.slugify = function () {
    return this.toLowerCase()
      .trim()
      .replace(/\ /, "-")
      .match(/[a-z0-9\-]/g)
      .join("")
  }),
  jQuery.fn.extend({
    adonNavScrollId: function () {
      return $(this).each(function () {
        return (
          (TARGET = $(this)),
          (targetText = TARGET.text().slugify()),
          (targetTop = TARGET.offset().top),
          TARGET.attr("id", targetText),
          $("a")._adonActivateLink(targetText, targetTop),
          TARGET
        )
      })
    },
    adonNavScrollLightup: function () {
      return $(this).each(function () {
        return (
          (TARGET = $(this)),
          (targetText = TARGET.text().slugify()),
          (targetTop = TARGET.offset().top),
          TARGET.attr("id", targetText),
          $("a")._adonActivateLightUp(targetText, targetTop),
          TARGET
        )
      })
    },
    _adonActivateLink: function (t, o) {
      return $(this).each(function () {
        return (
          (TAG = $(this)),
          t !== TAG.text().slugify() ||
            TAG.attr("href") ||
            (TAG.parent("li").addClass("adonNavScrollLink"),
            TAG.click(function () {
              $("html, body").animate({ scrollTop: o }, 1e3)
            })),
          TAG
        )
      })
    },
    _adonActivateLightUp: function (t, o) {
      return $(this).each(function () {
        return (
          (TAG = $(this)),
          t !== TAG.text().slugify() ||
            TAG.attr("href") ||
            ((tO = window.setTimeout(TAG._adonMyTestScrollPosition(o), 1e3)),
            $(window).scroll(function () {
              window.clearTimeout(tO),
                (tO = window.setTimeout(TAG._adonMyTestScrollPosition(o), 1e3))
            })),
          TAG
        )
      })
    },
    _adonMyTestScrollPosition: function (t) {
      ;(TAG = $(this)),
        (winY = $(window).scrollTop()) - 20 <= t &&
          winY + 20 >= t &&
          TAG._adonLightup(),
        winY + 50 > document.body.scrollHeight - window.innerHeight &&
          TAG._adonLightup()
    },
    _adonLightup: function () {
      return (
        (TAG = $(this)).parent("li").siblings().removeClass("adonnavactive"),
        TAG.parent("li").addClass("adonnavactive"),
        TAG
      )
    },
    _adonNavScrollToHeading: function () {
      return (TAG = $(this)), $("#" + TAG.text().slugify())
    },
  })
