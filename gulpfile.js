import gulp from "gulp"
// import browserSync from "browser-sync"
import concat from "gulp-concat"
import { deleteAsync } from "del"
import gulpRename from "gulp-rename"
import gulpUglify from "gulp-uglify"

// browserSync.create()

let { src, dest, watch, series, parallel } = gulp
let { rename } = gulpRename
let uglify = gulpUglify

let ROOTS = [
  "elioangels",
  "eliobones",
  // "eliofaithful",
  "elioflesh",
  // "elioolympians",
  // "eliorenaissance",
  "eliosin",
  "eliothing",
]

const FILES = {
  scssWatch: [
    "./eliosin/bushido/rei/dist/css/*",
    "./eliosin/sins/sloth/dist/css/*",
  ],
  jsPath: [
    "./eliosin/adon/js/adon/adonHideOnScrollDown.js",
    "./eliosin/adon/js/adon/adonHighLink.js",
    "./eliosin/adon/js/adon/adonNavScrollTo.js",
  ],
  publicPaths: ROOTS.map(r => `./${r}/public/**`),
}

// let taskIcons = done => {
//   return src(["./eliosin/icon/**/*"]).pipe(dest("public/icon/"))
//   done()
// }

let sinThemesIcons = sinTheme => {
  return done => {
    return src([`./eliosin/${sinTheme}/*/apple-touch-icon.png`]).pipe(
      dest(`./eliosin/public/${sinTheme}/`)
    )
    done()
  }
}

// For local testing, this moves the documentation stored in subfolders into
// the root public folder.
// let uplift = subfolder => {
//   return done => {
//     return src([`${subfolder}/public/**/*`]).pipe(dest(`public/${subfolder}/`))
//     done()
//   }
// }

// let deletedUplifted = done => {
//   return deleteAsync(ROOTS.map(r => `./public/${r}/`))
// }

let cssTask = cssTheme => {
  return done => {
    return src(`eliosin/${cssTheme}/dist/css/*`)
      .pipe(dest("dist/css")) // for local browsing of documentation
      .pipe(dest("public/dist/css")) // for gitlab pages
    done()
  }
}

let adonTask = done => {
  return src(FILES.jsPath)
    .pipe(concat("adons.js"))
    .pipe(uglify())
    .pipe(dest("dist/js")) // for local browsing of documentation
    .pipe(dest("public/dist/js")) // for gitlab pages
  // .pipe(browserSync.stream())
  done()
}

export const gitlab = parallel(
  sinThemesIcons("bushido"),
  sinThemesIcons("sins"),
  // taskIcons,
  cssTask("bushido/rei"),
  cssTask("sins/sloth"),
  adonTask
)

// export const localise = parallel(
//   uplift("elioangels"),
//   uplift("eliobones"),
//   uplift("elioflesh"),
//   uplift("eliosin"),
//   uplift("eliothing"),
//   gitlab
// )
//
// export const develop = series(localise)
// export const deploy = series(deletedUplifted, gitlab)

// export const watchTask = () => {
//   // browserSync.init({
//   //   server: {
//   //     baseDir: "./public/",
//   //   },
//   // })
//   watch(
//     FILES.scssWatch.concat(FILES.jsPath).concat(FILES.publicPaths),
//     develop
//   ).on("change", browserSync.reload)
// }

// export const devAndWatch = series(develop, watchTask)
// export delpublic
// export deploy
// export develop
// export watchTask
export default gitlab
// exports.delpublic = delpublic
// exports.deploy = deploy
// exports.develop = series(develop, watchTask)
// exports.default = exports.develop
